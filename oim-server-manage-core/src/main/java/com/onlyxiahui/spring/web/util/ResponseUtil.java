package com.onlyxiahui.spring.web.util;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

/**
 * date 2018-07-20 17:34:24<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
public class ResponseUtil {

	public static void putAllowOrigin(HttpServletResponse response) {
		// 跨域
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		// 跨域 Header
		response.setHeader("Access-Control-Allow-Methods", "*");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with,Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE,X-Token");

	}

	public static void writer(HttpServletResponse response, String text) {
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
			writer.println(text);
			writer.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != writer) {
				writer.close();
			}
		}
	}
}
